#!/usr/bin/env python
# coding: utf-8


import torch
import numpy as np
import torchvision
from torchvision import *
from PIL import Image, ImageOps
import json
from fpdf import FPDF
from datetime import date

import matplotlib.pyplot as plt
import time
import copy
import os
import cv2

def createPdf(diagnosisString):
    # save FPDF() class into a 
    # variable pdf
    pdf = FPDF()
    # Add a page
    pdf.add_page()
    #################################################### Date/Time
    pdf.set_font("Arial", size = 15) 
    today = date.today()
    pdf.text(150, 10, txt = f"Date: {today}")
    #################################################### Title
    pdf.set_font("Arial", size = 25) 
    pdf.text(60, 30, txt = "Result of dAIgnose prediction")
    pdf.set_text_color(255,0,0)
    pdf.set_font("Times", size = 15, style='I')
    pdf.text(55, 50, txt = diagnosisString)
    pdf.set_text_color(0,0,0)
    #################################################### Logo
    pdf.image("dAIgnoseLogo.png", 10, 5, 40)
    #################################################### Result
    pdf.set_xy(30, 40)
    pdf.cell(150, 30, border = 1)
    pdf.set_font("Arial", size = 15)
    pdf.text(35, 50, "Result:")
    #################################################### Notes
    pdf.set_xy(30, 80)
    pdf.set_font("Arial", size = 15)
    pdf.text(35, 90, txt = "Notes:") 
    pdf.cell(150, 150, border = 1, align='L')
    #################################################### Doctor Name and Signature
    pdf.text(30, 260, txt="............................")
    pdf.text(30, 275, txt="Doctor name and Signature")
    #################################################### Save
    pdf.output("diagnosis.pdf") 

def predict_new_image(image_tensor):
    input = image_tensor.to(device)
    output = model(input)
    _, index = torch.max(output,1)
    percentage = torch.nn.functional.softmax(output, dim=1)[0] * 100
    return percentage, index


#Set parameters
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
use_GPU=False #use_GPU=torch.cuda.is_available()

transfs = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(mean=154,std=66),
])

if __name__ == "__main__":
    import argparse
    from pathlib import Path
    parser = argparse.ArgumentParser()
    parser.add_argument("imPath", help="image file path",
                        default="/home/studio-lab-user/data/images_sample_restructured2/test/Atelectasis/00000080_001.png",
                        type=str)
    parser.add_argument("--modelPath", help="path to trained model",
                        default="/home/studio-lab-user/daignose/Diane-dev/resnet_trained_new.pt",
                        type=str)
    parser.add_argument("--labelsPath", help="path to class dict",
                        default="/home/studio-lab-user/daignose/Diane-dev/labels_keys.json",
                        type=str)
    parser.add_argument("--logoPath", help="path to the logo",
                        default="/home/studio-lab-user/daignose/Diane-dev/dAIgnoseLogo.jpg",
                        type=str)
    args = parser.parse_args()
    imPath, modelPath, labelsPath, logoPath = Path(args.imPath), Path(args.modelPath), Path(args.labelsPath), Path(args.logoPath)

    #Load model
    print('Loading AI model...', end=' ')
    model=torch.load(modelPath)
    model.eval()
    print('Done ! ')
    # Load data
    print('Opening image...', end=' ') 
    img = Image.open(imPath)
    print('Done ! ')
    print('Preparing image for prediction...', end=' ') 
    if len(np.shape(img))==3:
        img=ImageOps.grayscale(img)
    #to_pil = transforms.ToPILImage()
    #img = to_pil(img)
    img_tensor = transfs(img).to(device).unsqueeze(0)
    print('Done ! ')
    print('Running prediction')
    print('\n')
    perc,outputClass = predict_new_image(img_tensor)
    # Load mapping between output number and classes
    f = open(labelsPath)
    class_data = json.load(f)
    label = class_data[str(outputClass[0].item())]
    label = label.replace('_', ' ')
    percentage=perc[outputClass[0]].item()
    diagString="{} with probability {:.2f}%".format(label, percentage)
    print("AI-based diagnosis: {} with probability {:.2f}%".format(label, percentage))
    createPdf(diagString)
    data={"Class":label, "Percentage":percentage}
    with open('prediction.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False)
    #print('\n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n')
    
    