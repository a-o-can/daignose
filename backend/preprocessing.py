#!/usr/bin/env python

import cv2
import numpy as np
from PIL import Image
from random import randint
import os
    
class ImagePreprocessor:
    def __init__(self, blur=5, grain=False):
        self.blur = blur
        self.grain = grain
    def grain_filter(self, image):
        thickness = -1
        color = (0, 0, 0)
        h, w = image.shape[:2]
        xStart = randint(0, w)
        yStart = randint(0, h)
        if xStart + width > w:
            width = w
        if yStart + height > h:
            height = h
        startPoint = (int(xStart), int(xStart + width))
        endPoint = (int(yStart), int(yStart + height))
        imageBlackedOut = cv2.rectangle(image, startPoint, endPoint, color, thickness)
        return imageBlackedOut
    def __call__(self, image):
        if self.blur != 0:
            image = cv2.GaussianBlur(image, (self.blur,self.blur), 0)
        if self.grain:
            image = self.grain_filter(image)
        return image

if __name__ == "__main__":
    import argparse
    from pathlib import Path
    parser = argparse.ArgumentParser()
    parser.add_argument("--src", help="source folder",
                        default="/home/studio-lab-user/data/images_sample_resized/",
                        type=str)
    parser.add_argument("--dst", help="destination folder",
                        default="blurred_data/",
                        type=str)
    args = parser.parse_args()
    src, dst = Path(args.src), Path(args.dst)
    dst.mkdir(parents=True, exist_ok=True)
    imfilter = ImagePreprocessor()
    files = list(src.glob("*.png"))
    nAugmented = 1
    for file in files:
        srcim = src / file
        image = cv2.imread(str(src / file))
        image = imfilter(image)
        dstdir = str(dst / file.name)
        cv2.imwrite(dstdir, image)
        if nAugmented in np.arange(start = 0,stop = len(files), step = 1000):
            print(f"Files left: {len(files) - nAugmented}")
        nAugmented += 1