#!/usr/bin/env python
# coding: utf-8


import os
from glob import glob
import cv2


imSize=128

if __name__ == "__main__":
    import argparse
    from pathlib import Path
    parser = argparse.ArgumentParser()
    parser.add_argument("inpFolder", help="image folder path", type=str)
    parser.add_argument("outFolder", help="output folder destination", type=str)
    
    args = parser.parse_args()
    inpFolder, outFolder = Path(args.inpFolder), Path(args.outFolder)
    
    if not os.path.isdir(outFolder):
        os.mkdir(outFolder)
    
    imagesPath=glob(os.path.join(inpFolder, '*.png'))
    for imP in imagesPath:
        im = cv2.imread(os.path.join(inpFolder, imP))
        im2 = cv2.resize(im,(imSize,imSize))
        cv2.imwrite(os.path.join(outFolder, os.path.basename(imP)), im2)

    


