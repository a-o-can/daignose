# TUM.ai Makeathon 2022: Team dAIgnose
This project is the result of the team dAIgnose (team #03) for the 2022 TUM.ai Makeathon. The team members are:
- Ali Oğuz Can
- Dr. Sarah Homberg
- Andrea Lizzit
- Diego Montero
- Diane Tchibozo

## diagnosis inference
this package is needed to run the dAIgnose interface app.

### installation instructions
- clone the repository and run `python -m build`, or download the whell from the dist/ directory
- `sudo python -m pip install dist/<filename>.whl`
- check that the installation was successful:
	- if `inference.py --help` does not raise errors, the installation was successful

