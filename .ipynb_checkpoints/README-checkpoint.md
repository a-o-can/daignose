# TUM.ai Makeathon 2022: Team dAIgnose
This project is the result of the team dAIgnose (team #03) for the 2022 TUM.ai Makeathon. The team members are:
- Ali Oğuz Can
- Dr. Sarah Homberg
- Andrea Lizzit
- Diego Montero
- Diane Tchibozo

## Challenge: Multi-Class Image Classification
Using the ChestX-ray dataset from NIH Clinical Center, create an AI-driven assistant to help primary care physicians diagnose chest diseases in developing countries.

## Download data
The data used in this challenge is a reduced version of the full NIH dataset and is hosted on AWS. To install the data:
- Install AWS CLI ([instructions](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html ))
- In the command-line interpreter, navigate to the desired storage folder 
- Run the following command: “aws s3 sync --no-sign-request s3://challenge-healthcare-computer-vision-for-developing-world .”

In the ([backend](https://gitlab.com/a-o-can/daignose/-/tree/main/backend) branch, the NIH dataset can be pre-processed using a Python console and used to fine-tune a deep learning model. This model can then be applied to new images.
This backend is linked to a user interface built in the [daignose_app-newtree](https://gitlab.com/a-o-can/daignose/-/tree/main/daignose_app-newtree) and [package-master](https://gitlab.com/a-o-can/daignose/-/tree/main/package-master ) branches. This interface can be run directly by the user.


## 1. Backend branch: Model training and inference
#### Pre-processing
1. Run the resize_im.py script on the initial image folder to resize the 1024x1024 images into 128x128 images.
2. Run the preprocessing.py script to simulate low-quality data. 
3. Run the adapt_dataset_pytorch notebook to apply Contrast-Limited Adaptive Histogram Equalization to the images and restructure them into the necessary structure for model learning.

#### Model making
Predictions are made using a pre-trained ResNet18 model. 

To get the Resnet18 model and fine-tune it on a chosen dataset, run the test_pytorch_grayscale notebook. Running this notebook creates and saves the fine-tuned model (resnet_trained_new.pt) and the file mapping the output numbers to classes (labels_keys.json)

#### Inference
To run a prediction on a single image, run the inference.py script. It requires the input image path, the model (resnet_trained_new.pt) and the file mapping the output numbers to classes (labels_keys.json) and outputs a pdf with the diagnosis and its corresponding probability.

## 2. User interface for a user-friendly use
The Python files are linked to a user interface

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

